CREATE TABLE car (
    id INT NOT NULL,
    license_plate VARCHAR(2000) NOT NULL,
    color VARCHAR(2000) NOT NULL,
    PRIMARY KEY (id)
);