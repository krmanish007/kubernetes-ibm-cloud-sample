'use strict';

import App from './app';
const React = require('react');
const ReactDOM = require('react-dom');

export default class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {licensePlate: '', color: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  handleSubmit(event) {
	    fetch("createCar", {
           method: 'POST',
           headers: {
             'Accept': 'application/json',
             'Content-Type': 'application/json',
           },
           body: JSON.stringify({
               licensePlate: this.state.licensePlate,
               color: this.state.color,
             })
         })
    event.preventDefault();
    this.state = {licensePlate: '', color: ''};
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Name:
          <input type="text" name="licensePlate" onChange={this.handleChange}/>
          <input type="text" name="color" onChange={this.handleChange}/>
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}