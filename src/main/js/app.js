'use strict';

import NameForm from './createCar';
const React = require('react');
const ReactDOM = require('react-dom');

export default class App extends React.Component {

	constructor(props) {
		super(props);
            this.state = {
              error: null,
              isLoaded: false,
              items: []
            };
	}

	componentDidMount() {
	    fetch("all")
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                isLoaded: true,
                items: result
              });
            },
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
	}

	render() {
	    const { error, isLoaded, items } = this.state;
        if (error) {
          return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
          return <div>Loading takes 5 seconds...</div>;
        } else {
            return (
                <div>
                    <CarList cars={this.state.items}/>
                    <NameForm/>
                </div>
            )
        }
    }
}

class CarList extends React.Component{
	render() {
		const cars = this.props.cars.map(car => <Car car={car}/>);
		return (
			<table>
				<tbody>
					<tr>
						<th>Id</th>
						<th>licence plate</th>
						<th>color</th>
					</tr>
					{cars}
				</tbody>
			</table>
		)
	}
}

class Car extends React.Component{
	render() {
		return (
			<tr>
				<td>{this.props.car.id}</td>
				<td>{this.props.car.licensePlate}</td>
				<td>{this.props.car.color}</td>
			</tr>
		)
	}
}

ReactDOM.render(
	<App />,
	document.getElementById('react')
)
