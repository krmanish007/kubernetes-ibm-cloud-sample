package com.infogen.swagger.controller;

import com.infogen.swagger.domain.entity.Car;
import com.infogen.swagger.domain.entity.repository.CarRepository;
import java.util.List;
import java.util.Random;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ManageCarController {

    private final CarRepository carRepository;
    private static final Random RANDOM = new Random();

    public ManageCarController(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @GetMapping("/all")
    public List<Car> name() {
        return carRepository.findAll();
    }

    @PostMapping("/createCar")
    public void createCar(@RequestBody Car car) {
        System.out.println("car = " + car);
        car.setId(RANDOM.nextInt());
        carRepository.save(car);
    }
}
