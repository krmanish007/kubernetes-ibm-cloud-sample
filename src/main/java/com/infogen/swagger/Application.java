package com.infogen.swagger;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application {

    @RequestMapping("/name")
    public String name() {
        return "My name is Shreya";
    }

    @RequestMapping("/wife")
    public String wife() {
        return "My  wife's name is Shubhra";
    }

    @RequestMapping("/time")
    public String time() {
        final DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL)
              .withLocale(Locale.UK)
              .withZone(ZoneId.systemDefault());

        return "it is " + formatter.format(Instant.now());
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}

