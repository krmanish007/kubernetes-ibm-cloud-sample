package com.infogen.swagger.domain.entity.repository;

import com.infogen.swagger.domain.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, String> {

}
