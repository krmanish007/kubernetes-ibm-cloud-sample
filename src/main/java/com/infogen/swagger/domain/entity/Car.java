package com.infogen.swagger.domain.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Car {

    @Id
    private int id;
    private String licensePlate;
    private String color;

    public Car() {
    }

    public Car(int id, String licensePlate, String color) {
        this.id = id;
        this.licensePlate = licensePlate;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public String getColor() {
        return color;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Car{" +
              "id=" + id +
              ", license_plate='" + licensePlate + '\'' +
              ", color='" + color + '\'' +
              '}';
    }
}
